/* global document */

const updateWeather = function() {
    let wXHR = new XMLHttpRequest();
    wXHR.responseType = "json";
    wXHR.open("GET", "https://api.openweathermap.org/data/2.5/weather?q=Norderstedt,de&APPID=9564566babefcbf6505aa8c957a84eda");
    wXHR.onload = function() {
        if (wXHR.status === 200) {
            const r = wXHR.response;
            const weather = r.name + ": " + Math.round(r.main.temp - 273.15) +
                "C°, " + r.weather[0].description;
            document.getElementById("weather").textContent = weather;

            localStorage.setItem("weatherCheckTime", new Date().getTime());
            localStorage.setItem("weather", weather);
        }
    };
    wXHR.send();
}

let now = new Date().getTime();
let saved = localStorage.getItem("weatherCheckTime");
let savedWeather = localStorage.getItem("weather");
if (saved === null || savedWeather === null || now - saved > 10*60*1000) {
    updateWeather();
} else {
    document.getElementById("weather").textContent = savedWeather;
}
setInterval(updateWeather, 10*60*1000);



const weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday",
                "Thursday", "Friday", "Saturday"];
const months = ["January", "February", "March", "April",
              "May", "June", "July", "August",
              "September", "October", "November", "December"];

const eltWeekday = document.getElementById("weekday");
const eltDate = document.getElementById("date");
const eltTime = document.getElementById("time");
const updateTime = function() {
    const date = new Date();
    eltWeekday.textContent = weekdays[date.getDay()];
    eltDate.textContent = months[date.getMonth()] + " " + date.getDate();
    eltTime.textContent = date.toLocaleTimeString();
}

updateTime();
setInterval(updateTime, 200);

document.getElementById("getIP").onclick = function() {
    let ipXHR = new XMLHttpRequest();
    ipXHR.open("GET", "https://api.ipify.org");
    ipXHR.onload = function() {
        if (ipXHR.status === 200) {
            document.getElementById("ip").textContent = ipXHR.responseText;
        }
    };
    ipXHR.send();
};



const engines = {
    "=": "https://www.wolframalpha.com/input/?i=",
    "/": "https://www.reddit.com/r/",
    ":cpp ": "https://en.cppreference.com/mwiki/index.php?title=Special%3ASearch&search=",
    ":w ": "https://en.wikipedia.org/w/index.php?search=",
    ":wr ": "https://ru.wikipedia.org/w/index.php?search=",
    ":wd ": "https://de.wikipedia.org/w/index.php?search=",
    ":li ": "https://www.lingvolive.com/ru-ru/translate/en-ru/",
};
const searchBar = document.getElementById("search-bar");
searchBar.onkeyup = function(e) {
    if (e.keyCode === 13) {
        query = searchBar.value;
        let done = false;
        for (const [comm, eng] of Object.entries(engines)) {
            if (query.startsWith(comm)) {
                window.location.href = eng + encodeURIComponent(
                    query.substr(comm.length).trim()
                );
                done = true;
            }
        }
        if (!done) {
            window.location.href = "https://www.google.com/search?q="
                + encodeURIComponent(query);
        }
    }
};
