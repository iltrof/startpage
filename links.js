const categories = [{
    name: "reddit",
    mainURL: "https://www.reddit.com",
    color: "#64b5f6",
    links: [{
        name: "osugame",
        href: "https://www.reddit.com/r/osugame"
    },{
        name: "popular",
        href: "https://www.reddit.com/r/popular"
    },{
        name: "all",
        href: "https://www.reddit.com/r/all"
    }]
},{
    name: "uni",
    mainURL: "https://intranet.tuhh.de/editor/index.php",
    color: "#e57373",
    links: [{
        name: "stud.ip",
        href: "https://e-learning.tu-harburg.de/studip/dispatch.php/my_courses"
    },{
        name: "fileserv",
        href: "http://tuhh.fileserv.eu/index.php"
    },{
        name: "aiw",
        href: "https://cloud.aiw-ges.de/s/klausuren"
    },{
        name: "sos",
        href: "https://www.service.tuhh.de/qissos/rds?state=user&type=0"
    }]
},{
    name: "watch",
    mainURL: "",
    color: "#ab47bc",
    links: [{
        name: "youtube",
        href: "https://www.youtube.com"
    },{
        name: "twitch",
        href: "https://www.twitch.tv"
    },{
        name: "9anime",
        href: "https://www.9anime.to"
    }]
},{
    name: "ctf",
    mainURL: "",
    color: "#fff59d",
    links: [{
        name: "otw",
        href: "https://overthewire.org/wargames"
    },{
        name: "microcorruption",
        href: "https://www.microcorruption.com"
    },{
        name: "w3challs",
        href: "https://www.w3challs.com"
    }]
},{
    name: "code",
    mainURL: "",
    color: "#f48fb1",
    links: [{
        name: "github",
        href: "https://github.com/iltrof"
    },{
        name: "gitlab",
        href: "https://gitlab.com/iltrof"
    },{
        name: "codewars",
        href: "https://www.codewars.com"
    }]
},{
    name: "social",
    mainURL: "",
    color: "#aed581",
    links: [{
        name: "whatsapp",
        href: "https://web.whatsapp.com"
    },{
        name: "vk",
        href: "https://vk.com"
    },{
        name: "facebook",
        href: "https://www.facebook.com"
    }]
}]

const linkDiv = document.getElementById("links");

for (const cat of categories) {
    const category = document.createElement("p");
    category.className = "category category-" + cat.name;
    category.style.color = cat.color;

    const headerSpan = document.createElement("span");
    headerSpan.className = "cat-header-cont";

    if (cat.mainURL !== "") {
        const header = document.createElement("a");
        header.className = "cat-header";
        header.href = cat.mainURL;

        header.appendChild(document.createTextNode(cat.name));
        headerSpan.appendChild(header);
    } else {
        headerSpan.appendChild(document.createTextNode(cat.name));
    }

    category.appendChild(headerSpan);

    for (const lnk of cat.links) {
        const a = document.createElement("a");
        a.className = "cat-link";
        a.href = lnk.href;

        a.appendChild(document.createTextNode(lnk.name));
        category.appendChild(a);
    }

    linkDiv.appendChild(category);
    linkDiv.appendChild(document.createElement("br"));
}
